extends Node2D

var MenuScene = "res://main/MainMenu.tscn"


func _ready():
	$UI/MarginContainer/VBoxContainer/Container/Texte/Punktzahl.text =str( globals.Punktzahl)
	$UI/MarginContainer/VBoxContainer/Container/Texte/Verpasst.text = str(globals.Verpasst)
	$UI/MarginContainer/VBoxContainer/Container/Texte/Schwierigkeitsgrad.text = str(globals.difficultyText)
	$UI/MarginContainer/VBoxContainer/Container/Texte/Zeit.text = str(globals.initialTimeText)
	if OS.get_name() == "HTML":
		visible = false

func _on_StartGame_button_down():
	get_tree().change_scene(MenuScene)


func _on_EndGame_button_down():
	get_tree().quit()
