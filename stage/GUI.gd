extends MarginContainer

class_name GUI

onready var scoreLabel : Label = $HBoxContainer/ScoreElement/Score
onready var missedLabel : Label = $HBoxContainer/MissedElement/Missed
onready var timerLabel: Label = $HBoxContainer/PanelContainer/VBoxContainer/TimeLeft
onready var diffcultyLabel : Label = $HBoxContainer/PanelContainer/VBoxContainer/Stage
onready var gameTimer : Timer = $GameTimer
var score = 0
var missedCount = 0



func _ready():
	_initScore()
	_initMissedCount()
	gameTimer.wait_time = globals.GameTime
	gameTimer.start()

func _process(delta):
	timerLabel.text = str(round(gameTimer.time_left))

func _initScore():
	_setScoreLabelText()

func _initMissedCount():
	_setMissedLabelText()

func _setScoreLabelText():
	scoreLabel.text = str(score)

func _setMissedLabelText():
	missedLabel.text = str(missedCount)

func _on_roller_catched(value = 1):
	_increaseScore(value)
	_setScoreLabelText()

func _increaseScore(value = 1):
	score += value

func _on_roller_missed():
	_increaseMissCount()
	_setMissedLabelText()

func _set_stage_text(stage : int):
	diffcultyLabel.text = "Difficulty " + str(stage)

func _increaseMissCount():
	missedCount += 1

func setTimeLeft(value):
	var timeLeft = gameTimer.time_left
	gameTimer.stop()

	if timeLeft + value <= 0:
		gameTimer.set_wait_time(0.1)
	else:
		gameTimer.set_wait_time(timeLeft + value)

	gameTimer.start()
