extends Label


var COMBO5 = 'COMBO5'
var COMBO10 = 'COMBO10'
var COMBO30 = 'COMBO30'

var blink
export var texts = {
	'COMBO5':'+ 5 Sekunden',
	'COMBO10':'+ 10 Sekunden',
	'COMBO30':'+ 30 Sekunden',
}

func _ready():
	self_modulate = 0

func blinkText(text):
	self.text = text
	$AnimationPlayer.play("blink")


func blinkCombo(combo):
	text = texts[combo]
	$AnimationPlayer.play("blink")
	get_tree().call_group("ROOT", "shake")
	$ComboBonusSound.play()
