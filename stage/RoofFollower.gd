extends PathFollow2D

export (PackedScene) var RollerNode
export (float) var speed
export (float) var timeMin = 1.0
export (float) var timeMax = 5.0

export (float) var offsetMin = 0.5
export (float) var offsetmax = 0.95

export (bool) var flipped
var RootNode
var detached = false
var nextOffset : float
var time_to_next_roller : float
var _fire : bool = false
var SpawnTimer : Timer


func increase_difficulty():
	if timeMin > 0.1:
		timeMin -= 0.05
	
	if timeMax > 0.5:		
		timeMax -= 0.25
		
	speed += 0.05
	
func _getRandomTimeToNextRoller():
	return rand_range(timeMin,timeMax)

func _spawnRoller():
	var inst = RollerNode.instance()
	inst.flipped = self.flipped
	add_child(inst)
	return inst

func _getRandomOffset():
	return rand_range(offsetMin,offsetmax)

func _getSpawnTimer():
	SpawnTimer = get_parent().get_child(1)

func _ready():
	randomize()
	RootNode = get_tree().get_root()
	_spawnRoller()
	_getSpawnTimer()
	nextOffset = _getRandomOffset()
	SpawnTimer.wait_time = _getRandomTimeToNextRoller()
	_fire = true

func _process(delta):
	if not _fire:
		return

	if not detached:
		unit_offset += delta * speed
	else:
		var roller = get_child(0)
		var rollerPos = roller.get_global_transform()

		remove_child(roller)

		roller.detached = true
		roller.mode = RigidBody2D.MODE_RIGID
		unit_offset=0

		RootNode.add_child(roller)

		roller.set_global_transform(rollerPos)
		SpawnTimer.wait_time = _getRandomTimeToNextRoller()
		nextOffset = _getRandomOffset()

		SpawnTimer.start()
		detached = false
		_fire = false


	if unit_offset > nextOffset:
		detached = true



func _on_SpawnTimer_timeout():
	_spawnRoller()
	
	_fire = true
	SpawnTimer.stop()
	
