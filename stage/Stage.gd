extends Node2D

export (int) var DifficultyIncrease = 30
export (PackedScene) var Player
var GameOverScene = "res://gameOver/GameOver.tscn"
var _gui : GUI

var catched_in_a_row = 0
var timer
var blinker
var in_a_row_label
var timeElapsed = 0
var RoofFollower
var player

var pitchEffect

onready var Cam = $MainCamera


func on_SpeedBonusStop():
	AudioServer.set_bus_effect_enabled(1,0,false)

func on_SpeedBonusStart():
	AudioServer.set_bus_effect_enabled(1,0,true)


func shake():
	$MainCamera.shake(.5, 20, 15)

func _ready():
	pitchEffect = AudioEffectPitchShift.new()
	AudioServer.add_bus_effect(1, pitchEffect, 0)
	pitchEffect.pitch_scale = 1.3
	AudioServer.set_bus_effect_enabled(1,0,false)


	player = Player.instance()
	player.connect("SpeedBonusStart", self, "on_SpeedBonusStart")
	player.connect("SpeedBonusStop", self, "on_SpeedBonusStop")
	add_child(player)
	player.position = $Spawner.position

	add_to_group("ROOT")
	_gui = $GUI
	blinker = $GUI/HBoxContainer/PanelContainer/VBoxContainer/ComboElement/ComboLabel
	in_a_row_label = $GUI/HBoxContainer/PanelContainer/VBoxContainer/ComboElement/HBoxContainer/InARow
	RoofFollower = $Path2D/RoofFollower



	$MusicPlayer.stream = globals.sounds['bgm']

	$MusicPlayer.play()



func _process(delta):
	timeElapsed += delta
	if timeElapsed >= DifficultyIncrease:
		get_tree().call_group("Spawner", "increase_difficulty")
		_gui._set_stage_text(globals.Difficulty)
		RoofFollower.increase_difficulty()
		globals.Difficulty += 1
		timeElapsed = 0



func handleRollerCatched(bonus):
	var points = 1

	if bonus != null:
		match bonus._type:
			"POINTS":
				points+=bonus._value
				blinker.blinkText("Punkte + " + str(bonus._value))
			"TIME":
				_gui.setTimeLeft(bonus._value)
				blinker.blinkText("Zeit + " + str(bonus._value))
			"SPEED":
				player.applyBonus(10,bonus._type,bonus._value * 10)
				blinker.blinkText("Mehr Geschwindigkeit!")
				$MusicPlayer.get_stream_playback()
			"COMBO":
				for i in bonus._value:
					_handle_combo_increase()
				blinker.blinkText("Combo + " + str(bonus._value))
			"NONE":
				pass

	_gui._on_roller_catched(points)
	_handle_combo_increase()

func _handle_combo_increase():
	catched_in_a_row += 1
	if catched_in_a_row % 20 == 0 :
		_gui.setTimeLeft(30)
		blinker.blinkCombo(blinker.COMBO30)

	elif catched_in_a_row % 10 == 0:
		_gui.setTimeLeft(20)
		blinker.blinkCombo(blinker.COMBO10)

	elif catched_in_a_row % 5 == 0:
		_gui.setTimeLeft(5)
		blinker.blinkCombo(blinker.COMBO5)

	_handle_score_changed()

func handleMissedCountIncrease():
	_gui._on_roller_missed()
	catched_in_a_row = 0
	_gui.setTimeLeft(-1 * (globals.Difficulty / 2))

	_handle_score_changed()

func reload_scene():
	get_tree().reload_current_scene()

func _handle_score_changed():
	in_a_row_label.text = str(catched_in_a_row)

func _on_GameTimer_timeout():
	globals.Punktzahl = _gui.score
	globals.Verpasst = _gui.missedCount
	get_tree().change_scene(GameOverScene)

