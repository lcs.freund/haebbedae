extends RigidBody2D


var detached = false
export ( float ) var impulseForce = 1
var impulse : Vector2
var flipped = false
var bonus


func _ready():
	bonus = globals.getRandomBonus(3)
	_match_bonus()

	var r_x = randi() % 900 + 300
	var r_y = randi() % 900 + 300
	# save random values in impulse
	impulse = Vector2(r_x,r_y * -1 ) * impulseForce

	# invert x axis of vector if flipped
	if flipped:
		impulse = Vector2(impulse.x * -1, impulse.y)


	$AnimationPlayer.play("shake")

	# obtain sounds
	$OuchPlayer.stream = globals.getRandomOuch()
	$ParticlesCatch/SuccessPlayer.stream = globals.getSuccess()
	$JumpPlayer.stream = globals.getRandomJump()

func _match_bonus():
	var hat = globals.HatMap[bonus._type]
	if hat != null:
		var hatInstance = hat.instance()

		$Sprites/Hat.add_child(hatInstance)
		hatInstance.apply_scale(Vector2.ONE * 2)

# enter tree gets calles again if we attach the node to another parent.
func _enter_tree():
	if detached:
		apply_central_impulse(impulse)
		$AnimationPlayer.play("jump")
		$JumpPlayer.play()
		if flipped:
			$Sprites.apply_scale(Vector2(-1,1))


func _on_Collision_FloorController():
	$OuchPlayer.play()
	get_tree().call_group("ROOT", "handleMissedCountIncrease")
	$CollisionShape2D.queue_free()
	$OnMissedDespawnTimer.start()



func _dequeue():
	queue_free()

func _on_Collision_Player():
	$ParticlesCatch/SuccessPlayer.play()
	get_tree().call_group("ROOT", "handleRollerCatched", bonus)

	var particles = $ParticlesCatch
	particles.show()
	var timer = $ParticlesCatch/Timer
	particles.emitting = true
	var globalTransform = particles.get_global_transform()
	remove_child(particles)
	timer.start()
	get_tree().get_root().add_child(particles)
	particles.set_global_transform(globalTransform)

	queue_free()


func _on_RoofRoller_body_entered(body):
	if body.name == "FloorCollider":
		_on_Collision_FloorController()
	elif body.name == "HandBebe":
		_on_Collision_Player()
	else:
		if detached:
			$OuchPlayer.stream = globals.getRandomOuch()
			$OuchPlayer.play()


func _on_OnMissedDespawnTimer_timeout():
	_dequeue()
