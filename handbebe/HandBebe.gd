extends KinematicBody2D


export (bool) var can_jump = true
export (bool) var controllable = true

export (int) var run_speed
export (int) var jump_speed
export (int) var gravity

var sprites # buffer sprite container

enum {IDLE, RUN, JUMP_A, JUMP_B} # animation states
enum {LEFT, RIGHT} # direction
var velocity = Vector2()

var state # holds current animation state

var anim
var new_anim

var direction
var old_direction

func _ready():
	change_state(IDLE)
	direction = RIGHT
	$StepsPlayer.stream =globals.sounds['steps']
	$JumpPlayer.stream = globals.sounds['jump1']
	sprites = $Sprites
	$Sprites/SpeedParticles.set_emitting(false)


func change_state(new_state):
	state = new_state
	match state:
		IDLE:
			if $StepsPlayer.playing:
				$StepsPlayer.stop()
			new_anim = 'hbd_idle'

		RUN:
			if not $StepsPlayer.playing:
				$StepsPlayer.play()
			new_anim = 'hbd_run'

		JUMP_A:
			if $StepsPlayer.playing:
				$StepsPlayer.stop()
			new_anim = 'hbd_jump_1'
			$JumpPlayer.play()

		JUMP_B:
			if $StepsPlayer.playing:
				$StepsPlayer.stop()
			new_anim = 'hbd_jump_2'

func flip_hori():
	sprites.apply_scale(Vector2(-1,1))

func get_input():
	if not controllable:
		return

	old_direction = direction # determine if we need to flip

	velocity.x = 0
	var right = Input.is_action_pressed('ui_right')
	var left = Input.is_action_pressed('ui_left')
	var jump = Input.is_action_pressed('ui_select')

	if right:
		if is_on_floor():
			change_state(RUN)
		velocity.x += run_speed
		direction = RIGHT

	if left:
		if is_on_floor():
			change_state(RUN)
		velocity.x -= run_speed
		direction = LEFT

	if can_jump and jump and is_on_floor() :
		change_state(JUMP_A)
		velocity.y -= jump_speed

	if !right and !left and (state == RUN or state == JUMP_B):
		if is_on_floor():
			change_state(IDLE)

	if not is_on_floor() and velocity.y > (gravity / 8):
		change_state(JUMP_B)

func _process(delta):
	get_input()

	if direction != old_direction:
		flip_hori()

	if new_anim != anim:
		anim = new_anim
		$AnimationPlayer.play(anim)

func _physics_process(delta):
	if not is_on_floor():
		velocity.y += gravity * delta

	velocity = move_and_slide(velocity, Vector2(0, -1))

func applyBonus(time, property, value):
	var timer = Timer.new()
	timer.connect("timeout",self,"_on_bonus_timer_timeout", [property, value, timer])
	timer.wait_time = time
	match property:
		"SPEED":
			globals.appliedBonuses += 1
			run_speed += (value * 20)
			$Sprites/SpeedParticles.set_emitting(true)
			emit_signal("SpeedBonusStart")

	timer.start()
	add_child(timer)

func _on_bonus_timer_timeout(property, value, timer):
	match property:
		"SPEED":
			globals.appliedBonuses -= 1
			run_speed -= (value * 20)

			if globals.appliedBonuses == 0:
				$Sprites/SpeedParticles.set_emitting(false)
				emit_signal("SpeedBonusStop")

	timer.queue_free()

signal SpeedBonusStart
signal SpeedBonusStop