extends PopupMenu

func _ready():
	add_item("30 Sekunden", 0, 0)
	add_item("60 Sekunden", 1, 0)
	add_item("90 Sekunden", 2, 0)
	add_item("120 Sekunden", 3, 0)

func _on_TimeSelectionMenu_id_pressed(ID):
	match ID:
		0:
			globals.GameTime = 30
		1: 
			globals.GameTime = 60
		2: 
			globals.GameTime = 90
		3: 
			globals.GameTime = 120