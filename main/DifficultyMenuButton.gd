extends MenuButton

var popup

func _ready():
	popup = get_popup()
	popup.add_item("Leicht",0,0)
	popup.add_item("Mittel",1,0)
	popup.add_item("Schwer",2,0)
	popup.add_item("Harcdore",3,0)
	popup.add_item("SuperHardcore",4,0)

	popup.connect("id_pressed", self, "on_id_pressed")
	globals.bonusRollerProbability = 50
	globals.difficultyText = "Leicht"

func on_id_pressed(ID):
	match ID:
		0:
			globals.bonusRollerProbability = 50
			globals.Difficulty = 1
			globals.difficultyText = "Leicht"
		1:
			globals.bonusRollerProbability = 25
			globals.Difficulty = 1
			globals.difficultyText = "Mittel"
		2:
			globals.bonusRollerProbability = 10
			globals.Difficulty = 2
			globals.difficultyText = "Schwer"
		3:
			globals.bonusRollerProbability = 0
			globals.Difficulty = 1
			globals.difficultyText = "Harcdore"
		4:
			globals.bonusRollerProbability = 0
			globals.Difficulty = 10
			globals.difficultyText = "SuperHardcore"

	text = get_popup().get_item_text(ID)