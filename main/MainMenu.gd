extends Node2D

var MainScene = "res://stage/Stage.tscn"


func _ready():
	globals.Difficulty = 1

func _on_StartGame_button_down():
	get_tree().change_scene(MainScene)

func _on_EndGame_button_down():
	get_tree().quit()
