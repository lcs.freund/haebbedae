extends MenuButton



var popup

func _ready():
	popup = get_popup()
	popup.add_item("60 Sekunden",0,0)
	popup.add_item("90 Sekunden",1,0)
	popup.add_item("120 Sekunden",2,0)
	popup.connect("id_pressed", self, "on_id_pressed")
	globals.initialTimeText = "60 Sekunden"
	
		
func on_id_pressed(ID):
	match ID:
		0:
			globals.GameTime = 60			
			globals.initialTimeText = "60 Sekunden"
		1:
			globals.GameTime = 90
			globals.initialTimeText = "90 Sekunden"
		2:
			globals.GameTime = 120
			globals.initialTimeText = "120 Sekunden"
	
	text = get_popup().get_item_text(ID)