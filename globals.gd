extends Node

export var  GameTime : float = 60

export var  Difficulty : int = 1

var Bonus = load("res://pickup_bonus.gd")
var HatA = load("res://mini_handbebe/hats/HatA.tscn")
var HatB = load("res://mini_handbebe/hats/HatB.tscn")
var HatC = load("res://mini_handbebe/hats/HatC.tscn")
var HatD = load("res://mini_handbebe/hats/HatD.tscn")

var HatMap = {
	"COMBO": HatA,
	"POINTS": HatB,
	"TIME": HatC,
	"SPEED": HatD,
	"NONE": null
}



var difficultyText : String
var initialTimeText : String

var Punktzahl : int
var Verpasst : int

onready var sounds = {}

var appliedBonuses = 0

var bonusRollerProbability = 25

func flipCoin():
	var probability = bonusRollerProbability
	var map = []
	for i in 100:
		if i < probability:
			map.append(true)
		else:
			map.append(false)
	
	return map[getRandomRangeInt(0,100)]
	
func _ready():
	_load_sounds()
	
func getRandomBonus(value):
	return Bonus.new(value, _getRandomBonusType())

func _getRandomBonusType():
	var bonusTypes = Bonus.types.keys()
	
	if flipCoin():
		return bonusTypes[getRandomRangeInt(0,bonusTypes.size())]
	else:
		return bonusTypes[-1]

func _toggle_bus(id):
	AudioServer.set_bus_mute(id, !AudioServer.is_bus_mute(id))
	
func toggleMusic():
	_toggle_bus(AudioServer.get_bus_index("Music"))

func toggleSound():
	_toggle_bus( AudioServer.get_bus_index("Sounds"))

func _load_sounds():
	sounds['bgm'] = load("res://sounds/bgm.ogg")

	sounds['ouch1'] = load("res://sounds/selection/ouch1.wav")
	sounds['ouch2'] = load("res://sounds/selection/ouch2.wav")
	sounds['ouch3'] = load("res://sounds/selection/ouch3.wav")

	sounds['jump1'] = load("res://sounds/selection/jump1.wav")
	sounds['jump2'] = load("res://sounds/selection/jump2.wav")
	sounds['jump3'] = load("res://sounds/selection/jump3.wav")

	sounds['steps'] = load("res://sounds/selection/steps.wav")

	sounds['success'] = load("res://sounds/selection/success.wav")

func getSuccess():
	return sounds['success']

func getRandomOuch():
	match getRandomRangeInt(0,3):
		0:
			return sounds['ouch1']
		1:
			return sounds['ouch2']
		2:
			return sounds['ouch3']
	return sounds['ouch1']

func getRandomJump():
	match getRandomRangeInt(0,3):
		0:
			return sounds['jump1']
		1:
			return sounds['jump2']
		2:
			return sounds['jump3']
	return sounds['jump1']

func getStepsSound():
	return sounds['steps']

func getRandomRangeInt(from, to):
	return randi() % to + from